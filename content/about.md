---
hidden: true
---

## About me

My name is André Almeida, but everybody calls me
[`tony`](https://www.youtube.com/watch?v=zhl-Cs1-sG4). I'm currently based
in Latin America.

I'm a software developer, interested in free software, privacy and low level
programming.


## Contact

If you want to get in touch with me, please feel free to send a message to
my email address: andrealmeid@riseup.net

To encrypt your message, use my public key: `0XA4F4604BA7C940CE`,
[key file](/tony.asc).

_As above, from bellow_
